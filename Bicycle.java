//Michael Obadia 1937890
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private int maxSpeed;

    public Bicycle(String manufacturer, int numberGears, int maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + manufacturer + ", Number of Gears: " + numberGears + ", Max Speed: " + maxSpeed;
    }

    public String getManu() {
        return manufacturer;
    }

    public int getGears(){
        return numberGears;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }


}