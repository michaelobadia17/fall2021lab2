//Michael Obadia 1937890
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bArr = new Bicycle[4];
        bArr[0] = new Bicycle("Specialized", 7, 40);
        bArr[1] = new Bicycle("BMX", 3, 45);
        bArr[2] = new Bicycle("Totoya", 8, 55);
        bArr[3] = new Bicycle("No-Name Brand", 5, 25);
        listBikes(bArr);
    }

    public static void listBikes(Bicycle[] arr) {
        for(int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
